﻿using DapperOrm;
using Dapper;
using Medialink.Interfaces;
using RestSharp;
using System;
using System.Data;
using Interfaces;

namespace MediaLink.Lib
{
    public class MathWebClient: IDoMath
    {
        private IRestClient _restClient;
        private const string apiURL = "http://api.mathjs.org/v4/";
        public ICanExecuteSqlQuery _dapperContext = (ICanExecuteSqlQuery) new DapperContext();

        public MathWebClient()
        {
            _restClient = new RestClient(apiURL);
        }

        public int Add(int a, int b)
        {
            string sum = _restClient.Get(new RestRequest($"?expr={a}%2B{b}", Method.GET)).Content;

            //log the request
            Log(apiURL + $"?expr={a}%2B{b}");
            return Convert.ToInt32(sum);
        }

        public int Multiply(int a, int b)
        {
            string product = _restClient.Get(new RestRequest($"?expr={a}*{b}", Method.GET)).Content;

            //log the request
            Log(apiURL + $"?expr={a}*{b}");
            return Convert.ToInt32(product);
        }

        public Decimal Divide(int a, int b)
        {
            string quotient = _restClient.Get(new RestRequest($"?expr={a}/{b}", Method.GET)).Content;

            //log the request
            Log(apiURL + $"?expr={a}/{b}");
            return Convert.ToDecimal(quotient);
        }

        public int Log(string logLine)
        {           
            string query = "INSERT INTO Logs (time, logLine) VALUES(@timeP, @loglineP);";
            Func<IDbConnection, int> insertLogLineFunc = (c) =>
            {
                var result = c.Execute(query, new { timeP = DateTime.Now, loglineP = logLine });
                return result;
            };

            return _dapperContext.Execute(insertLogLineFunc); 
        }
    }
}
