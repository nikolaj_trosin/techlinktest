﻿using Castle.Windsor;
using Medialink.Interfaces;
using MediaLink.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Medialink.Api.Controllers
{
   
    public class MathController : ApiController
    {
        private IDoMath _doMath;
        public MathController()
        {
            ConfigureDiContainer(); 
        }

        private void ConfigureDiContainer()
        {
            var container = new WindsorContainer();
            container.Register(Castle.MicroKernel.Registration.Component.For<IDoMath>().ImplementedBy<MathWebClient>());
            _doMath = container.Resolve<IDoMath>();
        }

        [HttpGet]    
        public HttpResponseMessage Add(int a, int b)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(_doMath.Add(a, b).ToString()); //new StringContent(Convert.ToString(a + b));

            return response;
        }

        [HttpGet]
        public HttpResponseMessage Multiply(int a, int b)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(_doMath.Multiply(a, b).ToString());

            return response;
        }

        [HttpGet]
        public HttpResponseMessage Divide(int a, int b)
        {
            HttpResponseMessage response;
            if (b == 0)
            {
                response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                return response;
            }

            response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(_doMath.Divide(a, b).ToString());
            return response;
        }
    }
}
