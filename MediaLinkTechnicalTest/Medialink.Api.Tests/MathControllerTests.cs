﻿using Medialink.Api.Controllers;
using NUnit.Framework;
using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Net.Http;

namespace Medialink.Api.Tests
{
    [TestFixture]
    public class MathControllerTests
    {
        MathController mathController = new MathController();

        [Test]
        public void TestAdd()
        {
            string result = ReadResponse(mathController.Add(1,1));        
            Assert.AreEqual("2", result);
        }

        [Test]
        public void TestMultiply()
        {
            string  result = ReadResponse(mathController.Multiply(2, 3));
            Assert.AreEqual("6", result);
        }

        [Test]
        public void TestDivide()
        {
            string result = ReadResponse(mathController.Divide(20, 5));
            Assert.AreEqual("4", result);
        }


        [Test]
        public void TestDivideByZero()
        {
            var result = mathController.Divide(20, 0);
            Assert.AreEqual("InternalServerError", result.StatusCode.ToString());
        }

        private string ReadResponse(HttpResponseMessage httpResponseMessage)
        {
            var result = httpResponseMessage.Content.ReadAsStringAsync();
            return result.Result;
        }
    }
}
