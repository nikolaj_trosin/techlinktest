﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Interfaces;

namespace DapperOrm
{
    // Instantiates connection to database 
    public class DapperContext: ICanExecuteSqlQuery
    {

        readonly string CONNECTION_STRING;

        public DapperContext()
        {
            CONNECTION_STRING = "Data Source=xxx; initial catalog =xxx;Integrated Security=True;";
        }

        public T Execute<T>(Func<IDbConnection, T> query)
        {
            using (var connection = GetOpenConnection()) 
            {
                return query(connection);
            }
        }

        private IDbConnection GetOpenConnection()
        {
            var connection = new SqlConnection(CONNECTION_STRING);
            connection.Open();
            return connection;
        }
    }
}
