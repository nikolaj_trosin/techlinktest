﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediaLink.Lib;
using NUnit.Framework;
using NSubstitute;
using Interfaces;

namespace Medialink.Lib.Tests
{
   
    [TestFixture]
    public class MathWebClientTests
    {
        MathWebClient mathWebClientMoqDapper = new MathWebClient();
        List<int> rangeOfInputs = Enumerable.Range(1, 2).ToList();

        public MathWebClientTests()
        {
            UseNSubstitute();
        }

        private void UseNSubstitute()
        {
            mathWebClientMoqDapper._dapperContext = Substitute.For<ICanExecuteSqlQuery>();
        }

        [Test]
        public void TestAdd()
        {
            List<int> result = new List<int>();
            foreach(int i in rangeOfInputs)
            {
                result.Add(mathWebClientMoqDapper.Add(i, 1));
            }
            var expected = rangeOfInputs.Select(i => i + 1).ToList();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void TestMultiply()
        {
            List<int> result = new List<int>();
            foreach (int i in rangeOfInputs)
            {
                result.Add(mathWebClientMoqDapper.Multiply(i, 2));
            }
            var expected = rangeOfInputs.Select(i => i * 2).ToList();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void TestDivide()
        {
            List<decimal> result = new List<decimal>();
            foreach (int i in rangeOfInputs)
            {
                result.Add(mathWebClientMoqDapper.Divide(i, 2));
            }
            var expected = rangeOfInputs.Select(i => i / 2.0).ToList();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void TestLog()
        {
            MathWebClient mathWebClientMoqDapper = new MathWebClient();
            int result = mathWebClientMoqDapper.Log("test log line");
            Assert.True(result == 1);
        }
    }
}
