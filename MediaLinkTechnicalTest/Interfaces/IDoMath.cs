﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medialink.Interfaces
{
    public interface IDoMath
    {
        int Add(int a, int b);
        int Multiply(int a, int b);
        decimal Divide(int a, int b);
    }
}
