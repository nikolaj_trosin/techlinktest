﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Interfaces
{
    public interface ICanExecuteSqlQuery
    {
        T Execute<T>(Func<IDbConnection, T> query);
    }
}
