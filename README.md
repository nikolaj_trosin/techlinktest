
## Techlink test

Notes:

1. Data base connection string: *MediaLinkTechnicalTest\DapperOrm\DapperContext.cs Line 19*
2. api.mathweb.com does not work and was replaced with similar service: api.mathjs.org

---

## Tasks

1. Part 1. Configure a dependency injection container: *MathController.cs -> ConfigureDiContainer()*
2. Part 1. Use dapper to implement the log method: *MathWebClient.cs  ->  Log()*
3. Part 2. Use NUnit and a mocking framework such as NSubstitute: *MathWebClientTests.cs -> UseNSubstitute()*
4. Medialink.Api.Tests. *MathControllerTests.cs -> (TestAdd(), TestMultiply(), TestDivide(), TestDivideByZero())*
5. Medialink.Lib.Tests. *MathWebClientTests.cs -> (TestAdd(), TestMultiply(), TestDivide(), TestLog())*
